# Dockerfile
FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get -y  install wget gnupg2 && \
    bash -c 'echo "deb http://www.yade-dem.org/packages/ focal main" >> /etc/apt/sources.list' && \
    wget -O - http://www.yade-dem.org/packages/yadedev_pub.gpg | apt-key add - && \
    apt-get update && \
    apt-get -y  install yadedaily yade --no-install-recommends

RUN apt-get -y autoclean && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /var/cache/apt/archives/*deb

